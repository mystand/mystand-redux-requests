"use strict";

if (typeof exports !== "undefined") Object.defineProperty(exports, "babelPluginFlowReactPropTypes_proptype_HashType", {
  value: require("react").PropTypes.shape({})
});

// common

if (typeof exports !== "undefined") Object.defineProperty(exports, "babelPluginFlowReactPropTypes_proptype_ActionType", {
  value: require("react").PropTypes.shape({
    type: require("react").PropTypes.string.isRequired
  })
});